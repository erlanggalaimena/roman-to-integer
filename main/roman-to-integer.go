package main

import "fmt"

func main() {
	fmt.Println("Version 1 - Angga")
	fmt.Println(romanToInt("III"))
	fmt.Println(romanToInt("LVIII"))
	fmt.Println(romanToInt("MCMXCIV"))
	fmt.Println()

	fmt.Println("Version 2 - Smart People")
	fmt.Println(romanToIntV2("III"))
	fmt.Println(romanToIntV2("LVIII"))
	fmt.Println(romanToIntV2("MCMXCIV"))
}

func romanToInt(s string) int {
	var romanSymbol map[rune]int = map[rune]int{
		'I': 1,
		'V': 5,
		'X': 10,
		'L': 50,
		'C': 100,
		'D': 500,
		'M': 1000,
	}

	var result = 0

	for i := 0; i < len(s); i++ {
		if i < (len(s) - 1) {
			if rune(s[i]) == 'I' && rune(s[i+1]) == 'V' {
				result += 4

				i++

				continue
			} else if rune(s[i]) == 'I' && rune(s[i+1]) == 'X' {
				result += 9

				i++

				continue
			} else if rune(s[i]) == 'X' && rune(s[i+1]) == 'L' {
				result += 40

				i++

				continue
			} else if rune(s[i]) == 'X' && rune(s[i+1]) == 'C' {
				result += 90

				i++

				continue
			} else if rune(s[i]) == 'C' && rune(s[i+1]) == 'D' {
				result += 400

				i++

				continue
			} else if rune(s[i]) == 'C' && rune(s[i+1]) == 'M' {
				result += 900

				i++

				continue
			}
		}

		result += romanSymbol[rune(s[i])]
	}

	return result
}

func romanToIntV2(s string) int {
	result := 0

	var romanSymbol map[uint8]int = map[uint8]int{
		'I': 1,
		'V': 5,
		'X': 10,
		'L': 50,
		'C': 100,
		'D': 500,
		'M': 1000,
	}

	for i := 0; i < len(s); i++ {
		if i < len(s)-1 {
			if romanSymbol[s[i]] < romanSymbol[s[i+1]] {
				result -= romanSymbol[s[i]]
			} else {
				result += romanSymbol[s[i]]
			}
		} else {
			result += romanSymbol[s[i]]
		}
	}

	return result
}
